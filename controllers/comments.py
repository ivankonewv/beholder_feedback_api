import datetime as dt

from flask import abort, jsonify, make_response, request
from flask.views import MethodView

from utils.decorators import require_cursor
from utils.pagination import get_paginated_list
from utils.validators import (validate_comment_existence, validate_email,
                              validate_phone_number, validate_region_and_city)


class CommentsAPI(MethodView):
    """
    Comments view class
    """

    @require_cursor
    def get(self, cursor, comment_id=False):
        if not comment_id:
            """
            Get a paginated list of comments
            """
            cursor.execute(
                """
                SELECT id, name, phone_number, email, comment FROM comments;
                """
            )
            results = cursor.fetchall()
            if results:
                return jsonify(get_paginated_list(
                    results,
                    '/',
                    start=request.args.get('start', 1),
                    limit=request.args.get('limit', 4)
                ))
            return jsonify(error='there is no results')
        else:
            """
            Get a single comment
            """
            cursor.execute("""
                    SELECT id, name, comment, date_changed, region, city, version, phone_number, email
                    FROM comments
                    WHERE id=%s;
                    """, (comment_id, ))
            result = cursor.fetchone()
            if result:
                return jsonify(result)
            return abort(make_response(jsonify(error='requested comment not found', status=404), 404))

    @require_cursor
    def post(self, cursor):
        """
        Create new comment
        """
        first_name = request.form.get('first_name')
        last_name = request.form.get('last_name')
        middle_name = request.form.get('middle_name')
        comment = request.form.get('comment')
        phone_number = request.form.get('phone_number')
        email = request.form.get('email')
        region = request.form.get('region')
        city = request.form.get('city')
        version = request.form.get('version')
        date_changed = request.form.get('date_changed')
        if not date_changed:
            date_changed = dt.datetime.now()
        if not version:
            version = 0
        if not comment:
            return abort(make_response(jsonify(error='enter comment text', status=400), 400))
        try:
            name = first_name + ' ' + last_name + ' ' + middle_name
        except TypeError:
            raise abort(make_response(jsonify(error='please, specify your full name', status=400), 400))
        if validate_region_and_city(cursor, city, region):
            if validate_email(email) and validate_phone_number(phone_number):
                cursor.execute("""
                    INSERT INTO comments(name, comment, date_changed, phone_number, email, region, city, version)
                    VALUES(%s, %s, %s, %s, %s, %s, %s, %s);
                    """, (name, comment, date_changed, phone_number, email, region, city, version, ))
                return jsonify(status='successfully created')
            return abort(make_response(jsonify(error='invalid email or phone number', status=400), 400))
        return abort(make_response(jsonify(error='enter a valid region and city', status=400), 400))

    @require_cursor
    def put(self, cursor, comment_id: int):
        """
        Update comment
        """
        if validate_comment_existence(cursor, comment_id):
            comment = request.form.get('comment')
            if comment:
                cursor.execute("""
                        UPDATE comments
                        SET comment = %s, date_changed = %s, version = version + 1
                        WHERE id = %s;
                        """, (comment, dt.datetime.now(), comment_id, )
                      )
                return jsonify(status='successfully updated')
            return abort(make_response(jsonify(error='please, enter new comment text', status=400), 400))
        return abort(make_response(jsonify(error='requested comment not found', status=404), 404))

    @require_cursor
    def delete(self, cursor, comment_id: int):
        """
        Delete comment
        """
        if validate_comment_existence(cursor, comment_id):  # Check if comment exists
            cursor.execute("""
                    DELETE FROM comments WHERE id=%s;
                    """, (comment_id, ))
            return jsonify(status='successfully deleted')
        return abort(make_response(jsonify(error='requested comment not found', status=404), 404))
