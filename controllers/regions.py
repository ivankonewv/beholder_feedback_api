from flask import abort, jsonify, make_response, request
from flask.views import MethodView

from utils.decorators import require_cursor
from utils.validators import validate_region_existence


class RegionsAPI(MethodView):

    @require_cursor
    def get(self, cursor):
        """
        Get comments count by regions
        """
        cursor.execute("""
            SELECT DISTINCT regions.id, regions.region, count(c.comment) as comments_count 
            FROM regions 
            LEFT JOIN comments c on regions.region = c.region
            GROUP BY regions.id, regions.region;
        """)
        results = cursor.fetchall()
        if results:
            return jsonify(results)
        return abort(make_response(jsonify(error='there is no results', status=204), 204))

    @require_cursor
    def post(self, cursor):
        region = request.form.get('region')
        if region:
            if not validate_region_existence(cursor, region):
                cursor.execute("""
                INSERT INTO regions(region) VALUES (%s);
                """, (region, ))
                return jsonify(status='successfully created')
            return abort(make_response(jsonify(error='region already exists', status=409), 409))
        return abort(make_response(jsonify(error='please, specify the region', status=400), 400))
