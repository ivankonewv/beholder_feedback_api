from flask import abort, jsonify, make_response, request
from flask.views import MethodView

from utils.decorators import require_cursor
from utils.validators import validate_city_existence, validate_region_existence


class CitiesAPI(MethodView):

    @require_cursor
    def get(self, cursor, region_id):
        cursor.execute("""
        SELECT DISTINCT cities.id, cities.city, count(c.comment) as comments_count 
        FROM cities
        LEFT JOIN comments c on cities.city = c.city
        LEFT JOIN regions r on cities.region = r.region
        WHERE r.id=%s
        GROUP BY cities.id, cities.city;
        """, (region_id, ))
        results = cursor.fetchall()
        if results:
            return jsonify(results)
        return abort(make_response(jsonify(error='there is no results', status=204), 204))

    @require_cursor
    def post(self, cursor):
        city = request.form.get('city')
        region = request.form.get('region')
        if city and region:
            if not validate_city_existence(cursor, city, region):
                if validate_region_existence(cursor, region):
                    cursor.execute("""
                    INSERT INTO cities(city, region) VALUES (%s, %s);
                    """, (city, region, ))
                    return jsonify(status='successfully created')
                return abort(make_response(jsonify(error='not valid region', status=400), 400))
            return abort(make_response(jsonify(error='city already exists', status=409), 409))
        return abort(make_response(jsonify(error='please, specify the city and region', status=400), 400))

