from flask import abort


def get_paginated_list(results, url, start, limit):
    """
    Pagination.
    """
    start = int(start)
    limit = int(limit)
    count = len(results)
    if count < start or limit < 0:
        abort(404)

    obj = dict()
    obj['start'] = start
    obj['limit'] = limit
    obj['count'] = count

    if start == 1:
        obj['previous'] = ''
    else:
        start_copy = max(1, start - limit)
        limit_copy = start - 1
        obj['previous'] = url + f'?start={start_copy}&limit={limit_copy}'
    if start + limit > count:
        obj['next'] = ''
    else:
        start_copy = start + limit
        obj['next'] = url + f'?start={start_copy}&limit={limit}'
    obj['results'] = results[(start - 1):(start - 1 + limit)]
    return obj
