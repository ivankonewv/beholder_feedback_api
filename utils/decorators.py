from psycopg2.extras import RealDictCursor

from db.database import get_db


def require_cursor(f):
    """
    psycopg2 Cursor decorator.
    """
    def wrapper(*args, **kwargs):
        connection = get_db()
        try:
            cursor = connection.cursor(cursor_factory=RealDictCursor)
            return_val = f(cursor=cursor, *args, **kwargs)
        except Exception:
            connection.rollback()
            raise
        else:
            connection.commit()
        return return_val
    return wrapper
