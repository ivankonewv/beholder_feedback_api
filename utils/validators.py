import re


def validate_email(email):
    """
    Email validator.
    Format: abc@abc.abc
    :returns: If success: email:str
              If not success: False:bool
    """
    if email:
        if re.fullmatch(r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b', email):
            return email
        return False
    return True


def validate_phone_number(phone_number):
    """
    Phone number validator.
    Format: (000)12345678
            (000)1234567
            (0000)1234567
            (0000)12345678
    :returns: If success: phone_number:str
              If not success: False:bool
    """
    if phone_number:
        if re.match(r'^(\([0-9]{3,4}\) ?|[0-9]{3})[0-9]{7,8}$', phone_number):
            return phone_number
        return False
    return True


def validate_region_and_city(cursor, city, region):
    """
    Checks region and city existence.
    """
    cursor.execute("""
    SELECT city, region FROM cities;
    """)
    data = cursor.fetchall()
    if city and region:
        if data:
            for row in data:
                if city in row['city'] and region in row['region']:
                    print(row['city'], row['region'])
                    return [city, region]
        return False
    elif not city and not region:
        return True


def validate_region_existence(cursor, region):
    """
    Check region existence
    """
    cursor.execute("""
    SELECT * FROM regions WHERE region=%s;
    """, (region, ))
    data = cursor.fetchone()
    if data:
        return True
    return False


def validate_city_existence(cursor, city, region):
    """
    Check city existence
    """
    cursor.execute("""
    SELECT * FROM cities WHERE city=%s and region=%s;
    """, (city, region, ))
    data = cursor.fetchone()
    if data:
        return True
    return False


def validate_comment_existence(cursor, comment_id):
    """
    Checks comment existence.
    """
    cursor.execute("""
            SELECT * FROM comments WHERE id=%s;
            """, (comment_id,))
    data = cursor.fetchone()
    if data:
        return True
    return False
