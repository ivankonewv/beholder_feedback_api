import pytest

from app import app
from db.database import get_db, truncate


@pytest.fixture
def client():
    with app.test_client() as client:
        app.config['TESTING'] = True
        with app.app_context():
            get_db()
            truncate()
        yield client


def test_region_creation(client):
    """
    Region creation
    """

    rv = client.post('/add_region', data=dict(
        region='Test region'
    ))
    assert b'successfully created' in rv.data
    assert rv.status_code == 200


def test_exising_region_creating(client):
    rv = client.post('/add_region', data=dict(
        region='Test region'
    ))
    rv2 = client.post('/add_region', data=dict(
        region='Test region'
    ))
    assert b'successfully created' in rv.data
    assert rv.status_code == 200
    assert b'successfully created' not in rv2.data
    assert b'region already exists' in rv2.data
    assert rv2.status_code == 409


def test_region_without_name_creating(client):
    rv = client.post('/add_region')
    assert b'successfully created' not in rv.data
    assert b'please, specify the region' in rv.data
    assert rv.status_code == 400


def test_city_creation(client):
    client.post('/add_region', data=dict(
        region='Test region'
    ))
    rv = client.post('/add_city', data=dict(
        region='Test region',
        city='Test city'
    ))
    assert b'successfully created' in rv.data
    assert rv.status_code == 200


def test_city_creation_with_not_valid_region(client):
    rv = client.post('/add_city', data=dict(
        region='Test region',
        city='Test city'
    ))
    assert b'successfully created' not in rv.data
    assert b'not valid region' in rv.data
    assert rv.status_code == 400


def test_city_creation_without_region(client):
    rv = client.post('/add_city', data=dict(
        city='Test city'
    ))
    assert b'successfully created' not in rv.data
    assert b'please, specify the city and region' in rv.data
    assert rv.status_code == 400


def test_city_creation_without_city(client):
    rv = client.post('/add_city', data=dict(
        region='Test region',
    ))
    assert b'successfully created' not in rv.data
    assert b'please, specify the city and region' in rv.data
    assert rv.status_code == 400


def test_city_already_exist_creation(client):
    client.post('/add_region', data=dict(
        region='Test region'
    ))
    client.post('/add_city', data=dict(
        region='Test region',
        city='Test city'
    ))

    rv = client.post('/add_city', data=dict(
        region='Test region',
        city='Test city'
    ))
    assert b'successfully created' not in rv.data
    assert b'city already exists' in rv.data
    assert rv.status_code == 409

