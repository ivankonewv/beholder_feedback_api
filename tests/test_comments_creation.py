import json

import pytest

from app import app
from db.database import get_db


def get_data():
    data = open('fixtures/test1.json', 'r')
    result = json.load(data)
    for row in result:
        records = row['records']
        yield records


@pytest.fixture
def client():
    with app.test_client() as client:
        with app.app_context():
            get_db()
        yield client


@pytest.mark.parametrize('fixture', get_data())
def test_comment_creation(fixture, client):
    """
    Comment creation
    """
    for record in fixture:
        rv = client.post('/', data=dict(
            first_name=record['name'].split()[0],
            last_name=record['name'].split()[1],
            middle_name=record['name'].split()[2],
            date_changed=record['date_changed'],
            version=record['version'],
            comment=record['comment']
        ))
        assert b'successfully created' in rv.data
        assert rv.status_code == 200


@pytest.mark.parametrize('fixture', get_data())
def test_comment_creation_without_comment_text(fixture, client):
    for record in fixture:
        rv = client.post('/', data=dict(
            first_name=record['name'].split()[0],
            last_name=record['name'].split()[1],
            middle_name=record['name'].split()[2],
            date_changed=record['date_changed'],
            version=record['version']
        ))
        assert b'successfully created' not in rv.data
        assert b'enter comment text' in rv.data
        assert rv.status_code == 400


@pytest.mark.parametrize('fixture', get_data())
def test_comment_creation_with_name(fixture, client):
    for record in fixture:
        #  Creation with full name
        rv = client.post('/', data=dict(
            first_name=record['name'].split()[0],
            last_name=record['name'].split()[1],
            middle_name=record['name'].split()[2],
            date_changed=record['date_changed'],
            version=record['version'],
            comment=record['comment']
        ))
        assert b'successfully created' in rv.data
        assert rv.status_code == 200


@pytest.mark.parametrize('fixture', get_data())
def test_comment_creation_without_middle_name(fixture, client):
    for record in fixture:
        rv = client.post('/', data=dict(
            first_name=record['name'].split()[0],
            last_name=record['name'].split()[1],
            date_changed=record['date_changed'],
            version=record['version'],
            comment=record['comment']
        ))
        assert b'successfully created' not in rv.data
        assert b'please, specify your full name' in rv.data
        assert rv.status_code == 400


@pytest.mark.parametrize('fixture', get_data())
def test_comment_creation_with_email_and_phone_number(fixture, client):
    for record in fixture:
        rv = client.post('/', data=dict(
            first_name=record['name'].split()[0],
            last_name=record['name'].split()[1],
            middle_name=record['name'].split()[2],
            date_changed=record['date_changed'],
            version=record['version'],
            comment=record['comment'],
            email='test@test.com',
            phone_number='(1234)1234567'
        ))
        assert b'successfully created' in rv.data
        assert rv.status_code == 200


@pytest.mark.parametrize('fixture', get_data())
def test_comment_creation_with_phone_number(fixture, client):
    for record in fixture:
        rv = client.post('/', data=dict(
            first_name=record['name'].split()[0],
            last_name=record['name'].split()[1],
            middle_name=record['name'].split()[2],
            date_changed=record['date_changed'],
            version=record['version'],
            comment=record['comment'],
            phone_number='(1234)1234567'
        ))
        assert b'successfully created' in rv.data
        assert rv.status_code == 200


@pytest.mark.parametrize('fixture', get_data())
def test_comment_creation_with_email(fixture, client):
    for record in fixture:
        rv = client.post('/', data=dict(
            first_name=record['name'].split()[0],
            last_name=record['name'].split()[1],
            middle_name=record['name'].split()[2],
            date_changed=record['date_changed'],
            version=record['version'],
            comment=record['comment'],
            email='test@test.com'
        ))
        assert b'successfully created' in rv.data
        assert rv.status_code == 200

@pytest.mark.parametrize('fixture', get_data())
def test_comment_email_validation(fixture, client):
    for record in fixture:
        rv = client.post('/', data=dict(
            first_name=record['name'].split()[0],
            last_name=record['name'].split()[1],
            middle_name=record['name'].split()[2],
            date_changed=record['date_changed'],
            version=record['version'],
            comment=record['comment'],
            email='test@test.',
            phone_number='(1234)1234567'
        ))
        assert b'successfully created' not in rv.data
        assert b'invalid email or phone number' in rv.data
        assert rv.status_code == 400


@pytest.mark.parametrize('fixture', get_data())
def test_comment_phone_number_validation(fixture, client):
    for record in fixture:
        rv = client.post('/', data=dict(
            first_name=record['name'].split()[0],
            last_name=record['name'].split()[1],
            middle_name=record['name'].split()[2],
            date_changed=record['date_changed'],
            version=record['version'],
            comment=record['comment'],
            email='test@test.com',
            phone_number='(12341234567'
        ))
        assert b'successfully created' not in rv.data
        assert b'invalid email or phone number' in rv.data
        assert rv.status_code == 400


@pytest.mark.parametrize('fixture', get_data())
def test_comment_email_and_phone_number_validation(fixture, client):
    for record in fixture:
        rv = client.post('/', data=dict(
            first_name=record['name'].split()[0],
            last_name=record['name'].split()[1],
            middle_name=record['name'].split()[2],
            date_changed=record['date_changed'],
            version=record['version'],
            comment=record['comment'],
            email='test@test',
            phone_number='(12341234567'
        ))
        assert b'successfully created' not in rv.data
        assert b'invalid email or phone number' in rv.data
        assert rv.status_code == 400


@pytest.mark.parametrize('fixture', get_data())
def test_creation_with_region_and_city(fixture, client):
    client.post('/add_region', data=dict(
        region='Russia'
    ))
    client.post('/add_city', data=dict(
        region='Russia',
        city='Moscow'
    ))

    for record in fixture:
        rv = client.post('/', data=dict(
            first_name=record['name'].split()[0],
            last_name=record['name'].split()[1],
            middle_name=record['name'].split()[2],
            date_changed=record['date_changed'],
            version=record['version'],
            comment=record['comment'],
            region='Russia',
            city='Moscow'
        ))
        assert b'successfully created' in rv.data
        assert rv.status_code == 200


@pytest.mark.parametrize('fixture', get_data())
def test_creation_with_not_valid_region(fixture, client):
    for record in fixture:
        rv = client.post('/', data=dict(
            first_name=record['name'].split()[0],
            last_name=record['name'].split()[1],
            middle_name=record['name'].split()[2],
            date_changed=record['date_changed'],
            version=record['version'],
            comment=record['comment'],
            region='Not valid region',
            city='Moscow'
        ))
        assert b'successfully created' not in rv.data
        assert b'enter a valid region and city' in rv.data
        assert rv.status_code == 400



@pytest.mark.parametrize('fixture', get_data())
def test_creation_with_not_valid_city(fixture, client):
    for record in fixture:
        rv = client.post('/', data=dict(
            first_name=record['name'].split()[0],
            last_name=record['name'].split()[1],
            middle_name=record['name'].split()[2],
            date_changed=record['date_changed'],
            version=record['version'],
            comment=record['comment'],
            region='Russia',
            city='Not valid city'
        ))
        assert b'successfully created' not in rv.data
        assert b'enter a valid region and city' in rv.data
        assert rv.status_code == 400


@pytest.mark.parametrize('fixture', get_data())
def test_creation_without_region_and_city(fixture, client):
    for record in fixture:
        rv = client.post('/', data=dict(
            first_name=record['name'].split()[0],
            last_name=record['name'].split()[1],
            middle_name=record['name'].split()[2],
            date_changed=record['date_changed'],
            version=record['version'],
            comment=record['comment'],
        ))
        assert b'successfully created' in rv.data
        assert b'enter a valid region and city' not in rv.data
        assert rv.status_code == 200
