from flask import Flask

from db.database import create_tables, drop_tables
from routes.routes import mod

#  TODO
app = Flask(__name__)

app.register_blueprint(mod)


if __name__ == '__main__':
    # drop_tables()
    create_tables()
    app.run(port=8000)
