Тестовое задание:   
============================

API приложения по учету комментариев

1 база данных postgresql
-------------------------------------

- создать sql скрипт с описанием таблиц
- при запуске приложения проверять наличие БД
- при отсутствии БД - сгенерировать ее программно на основании sql скрипта

2 wsgi приложение
----------------------------------------------------------------------------------------------
- использовать только стандартные  библиотеки и модули python 3.9,  psycopg2, requests, pytest
- виртуальное окружение через virtualenv, virtualenvwrapper, 
- пакеты - pip, pyenv или poetry
- контейнеризация - docker + docker-compose
- CRUD для ajax реквестов - выборка, добавление , удаление, изменение отзывов пользователей
- отчет "Распределение количества отзывов по регионам" и "Распределение количества отзывов по городам региона", GET-запросы, в последний параметром передается код региона
- респонс в json формате
- при получении ошибки  - возврат кода и описание ошибки в json формате
- выборка списка отзывов должна поддерживать страничную пагинацию
- интеграционные тесты а именно: начало сессии - пишутся данные из фикстур в базу, далее запускается сессия приложения в отдельном потоке и через http-клиент вызывается тестируемый эндпоинт приложения, полученные данные сравниваются с шаблонами которые передается в тестовую функцию декоратором `pytest.mark.parametrize`
- фикстуры для тестов хранятся в каталоге fixture, в формате 

```json
[
  {
    "table": "comment",
    "records": [
      {
        "id": 1,
        "name": "bla bla bla",
        "comment": "bla bla bla",
        "date_changed": "2021-04-02T20:35:57",
        "version": 0,
      },
      {
        "id": 2,
        "name": "bla bla bla 2",
        "comment": "bla bla bla 2",
        "date_changed": "2021-04-04T20:35:57",
        "version": 1,
      }
    ]
  }
]
```

- репозиторий проекта в гитлабе
- CI/CD cкрипт с выполнением тестов

Скриншоты
--------------------------------------------------------------------
![001](https://gitlab.com/elston/beholder_nodejs_api/-/raw/master/screenshots/001.png "001")
![002](https://gitlab.com/elston/beholder_nodejs_api/-/raw/master/screenshots/002.png "002")
![003](https://gitlab.com/elston/beholder_nodejs_api/-/raw/master/screenshots/003.png "003")
![005](https://gitlab.com/elston/beholder_nodejs_api/-/raw/master/screenshots/005.png "005")
![006](https://gitlab.com/elston/beholder_nodejs_api/-/raw/master/screenshots/006.png "006")
![007](https://gitlab.com/elston/beholder_nodejs_api/-/raw/master/screenshots/007.png "007")
![009](https://gitlab.com/elston/beholder_nodejs_api/-/raw/master/screenshots/009.png "009")
![010](https://gitlab.com/elston/beholder_nodejs_api/-/raw/master/screenshots/010.png "010")
![011](https://gitlab.com/elston/beholder_nodejs_api/-/raw/master/screenshots/011.png "011")
![012](https://gitlab.com/elston/beholder_nodejs_api/-/raw/master/screenshots/012.png "012")
