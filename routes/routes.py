from flask import Blueprint

from controllers.cities import CitiesAPI
from controllers.comments import CommentsAPI
from controllers.regions import RegionsAPI

comments_view = CommentsAPI.as_view('comments_api')
regions_view = RegionsAPI.as_view('regions_api')
cities_view = CitiesAPI.as_view('cities_api')


mod = Blueprint('comments', __name__, url_prefix='/')


mod.add_url_rule('/', defaults={'comment_id': None}, view_func=comments_view, methods=['GET'])
mod.add_url_rule('/', view_func=comments_view, methods=['POST'])
mod.add_url_rule('/<int:comment_id>', view_func=comments_view, methods=['GET', 'PUT', 'DELETE'])


mod.add_url_rule('/stat', view_func=regions_view, methods=['GET'])
mod.add_url_rule('/add_region', view_func=regions_view, methods=['POST'])

mod.add_url_rule('/stat/region/<int:region_id>', view_func=cities_view, methods=['GET'])
mod.add_url_rule('/add_city', view_func=cities_view, methods=['POST'])
