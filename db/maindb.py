def set_up_tables():
    create_comments_table = """
    CREATE TABLE IF NOT EXISTS comments (
        id serial PRIMARY KEY,
        name VARCHAR(255) NOT NULL,
        email VARCHAR(255),
        comment VARCHAR(1000) NOT NULL,
        date_changed TIMESTAMP DEFAULT now(),
        region VARCHAR(55) NULL references regions,
        city VARCHAR(55) NULL references cities,
        phone_number VARCHAR(55),
        version INTEGER NOT NULL 
    );
    """
    create_regions_table = """
    CREATE TABLE IF NOT EXISTS regions (
        id serial UNIQUE,
        region VARCHAR(255) NOT NULL UNIQUE PRIMARY KEY 
    );
    """
    create_cities_table = """
    CREATE TABLE IF NOT EXISTS cities (
        id serial UNIQUE,
        city VARCHAR(255) NOT NULL UNIQUE PRIMARY KEY ,
        region VARCHAR(255) NOT NULL references regions
    )
    """
    return [create_regions_table, create_cities_table, create_comments_table]


def drop_all_tables():
    drop_comments = """
    DROP TABLE IF EXISTS comments CASCADE 
    """
    drop_regions = """
    DROP TABLE IF EXISTS regions CASCADE 
    """
    drop_cities = """
    DROP TABLE IF EXISTS cities CASCADE 
    """
    return [drop_regions, drop_cities, drop_comments]


def truncate_tables():
    truncate_comments = """
    TRUNCATE TABLE comments CASCADE;
    """
    truncate_regions = """
        TRUNCATE TABLE regions CASCADE;
    """
    truncate_cities = """
        TRUNCATE TABLE cities CASCADE;
    """
    return [truncate_regions, truncate_cities, truncate_comments]
