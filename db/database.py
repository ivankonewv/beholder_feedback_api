from psycopg2.pool import SimpleConnectionPool

from .maindb import drop_all_tables, set_up_tables, truncate_tables

pool = SimpleConnectionPool(1, 20,
                            database='beholder',
                            user='postgres',
                            password='postgres',
                            host='beholder_test_db_1',
                            port='5432'
                            )


def get_db():
    connection = pool.getconn()
    pool.putconn(connection)
    return connection


def create_tables():
    conn = get_db()
    cursor = conn.cursor()
    tables = set_up_tables()
    for table in tables:
        cursor.execute(table)
        conn.commit()


def drop_tables():
    conn = get_db()
    cursor = conn.cursor()
    tables = drop_all_tables()
    for table in tables:
        cursor.execute(table)
        conn.commit()


def truncate():
    conn = get_db()
    cursor = conn.cursor()
    tables = truncate_tables()
    for table in tables:
        cursor.execute(table)
        conn.commit()

